package entidades;

public final class ContaEmpresa extends Conta {

	private Double limiteEmprestimo;

	public ContaEmpresa() {
		super();
	}

	public ContaEmpresa(Integer numero, String titular, Double saldo, Double limiteEmprestimo) {
		super(numero, titular, saldo);
		this.limiteEmprestimo = limiteEmprestimo;
	}

	public Double getLimiteEmprestimo() {
		return limiteEmprestimo;
	}

	public void setLimiteEmprestimo(Double limiteEmprestimo) {
		this.limiteEmprestimo = limiteEmprestimo;
	}

	public void emprestimo(Double quantidade) {
		if (quantidade <= limiteEmprestimo) {
			deposito(quantidade);
		}
	}

	@Override
	public void saque(double quantidade) {
		saldo -= quantidade;
	}
	
	@Override
	public void deposito(double quantidade) {
		super.deposito(quantidade);
		saldo += 2.0;
	}

}
