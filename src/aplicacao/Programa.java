package aplicacao;

import entidades.Conta;
import entidades.ContaEmpresa;

public class Programa {

	public static void main(String[] args) {
		
		Conta c = new Conta(1001, "Leandro", 1000.0);
		
		c.saque(200.00);
		
		System.out.println(c.getSaldo());
		
		Conta c2 = new ContaEmpresa(1002, "Pamela", 1000.0, 5000.0);
		
		c2.saque(200.00);
		
		System.out.println(c2.getSaldo());
		
		Conta c3 = new ContaEmpresa(1003, "j�o", 1000.0, 5000.0);
		
		c3.deposito(200.0);
		
		System.out.println(c3.getSaldo());
	}

}
